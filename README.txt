This package provides a means to view data from ELM-327 based devices 
(anything that supports the AT command set) on a HUD system using python.

For installation:
sudo python setup.py install

To Run:
1. sudo python carhud_server.py start
2. python carhud_text_client.py

If you have the projector with associated control buttons:
sudo python hudprojectorcontroller.py
