#!/usr/bin/env python

"""
CARHUD DUAL 7 SEGMENT LED DISPLAY - TEST
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pycarhud

7-segment LED based interface for the CARHUD system

This requires the following code from Adafruit:
  https://github.com/adafruit/Adafruit_Python_LED_Backpack
  https://github.com/adafruit/Adafruit_Python_GPIO

This requires the following for accessing the GPIO
  RPi-GPIO

CHANGELOG:
 01/2/2016 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
import time
import os
import sys
import elm327
import json
import threading
import Queue
from Adafruit_LED_Backpack import SevenSegment
import RPi.GPIO as GPIO
import random

# GLOBALS
# GPIO PINS - RASPBERRY PI REV A MODEL B
UP = 23    # INCREASE BRIGHTNESS
DOWN = 24  # DECREASE BRIGHTNESS
MODE = 25  # CHANGE SECOND 7 SEGMENT DATA
# SLEEP VALUES
RPGIOSLEEP = 0.1
# BRIGHTNESS LEVELS
MINBRIGHTNESS = 0
MAXBRIGHTNESS = 15

# QUEUE TO HANDLE BUTTON PRESSES
ButtonPress = Queue.Queue(1)

# I2C ADDRESSES FOR THE ADAFRUIT LED MATRICES AND 7-SEGMENT DISPLAYS
# THE I2C CONTROLLER ALLOWS FOR 4 OF THE
# DEVICES ON THE BUS 0x70 TO 0x73
# http://learn.adafruit.com/adafruit-led-backpack/0-8-8x8-matrix
# http://learn.adafruit.com/adafruit-led-backpack/changing-i2c-address
LEDADDRESSES = [0x70, 0x71]

# GPIO CALLBACKS
def increment(channel):
    if not ButtonPress.full():
        sys.stdout.write('we want to increase brightness')
        ButtonPress.put("INC")

def decrement(channel):
    if not ButtonPress.full():
        sys.stdout.write('we want to decrease brightness')
        ButtonPress.put("DEC")

def toggle(channel):
    if not ButtonPress.full():
        sys.stdout.write('we want to change mode')
        ButtonPress.put("TGL")

# GPIO SETUP FUNCTION
def SetupGPIO():
    # SETUP WIRING PI PIN SETUP
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()

    # SETUP OUR GPIO MODES
    GPIO.setup(UP, GPIO.IN)
    GPIO.setup(DOWN, GPIO.IN)
    GPIO.setup(MODE, GPIO.IN)

    # SETUP THE CALLBACKS
    GPIO.add_event_detect(UP, GPIO.FALLING, callback=increment, bouncetime=250)
    GPIO.add_event_detect(DOWN, GPIO.FALLING, callback=decrement, bouncetime=250)
    GPIO.add_event_detect(MODE, GPIO.FALLING, callback=toggle, bouncetime=250)

# FAKE ELM DEVICE FOR TEST
class FakeELM327(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.obdrespqueuelist = [Queue.Queue(1),Queue.Queue(1)]
        self.mpgqueue = Queue.Queue(1)
        self.sleeprate = 0.95
        self.dead = False

    def kill(self):
        self.dead = True

    def run(self):
        while not self.dead:
            print "GENERATING RANDOM DATA"
            # GENERATE RANDOM DATA
            vss = random.randint(0,150)+(random.randint(0,10) / 10.0)
            rpm = random.randint(0,8000)*1.0
            mpg = random.randint(0,99)+(random.randint(0,10) / 10.0)
            print vss, rpm, mpg

            # PUT DATA INTO THE QUEUES
            # WILL NOT BE SIMULATING THE ENTIRE ELM327 CLASS RESPONSE
            print "QUEUE 0 FULL", self.obdrespqueuelist[0].full()
            if not self.obdrespqueuelist[0].full():
                resp = {"name": "vss","response": vss}
                self.obdrespqueuelist[0].put(resp)

            print "QUEUE 1 FULL", self.obdrespqueuelist[1].full()
            if not self.obdrespqueuelist[1].full():
                resp = {"name": "rpm","response": rpm}
                self.obdrespqueuelist[1].put(resp)

            print "MPG QUEUE FULL", self.mpgqueue.full()
            if not self.mpgqueue.full():
                resp = {"name": "mpg","response": mpg}
                self.mpgqueue.put(resp)

            # SLEEP
            time.sleep(self.sleeprate)

# HUD CLASS
class HUD:
    def __init__(self, btnqueue, i2cbusmode=1, prisegaddr=0x70, secsegaddr=0x71):
        # CLASS VARIABLES
        self.dead = False  # MAIN LOOP CONTROL VARIABLE
        self.initialread = True
        self.i2cbusmode = i2cbusmode
        self.prisegaddr = prisegaddr
        self.secsegaddr = secsegaddr
        self.seg1 = SevenSegment.SevenSegment(address=self.prisegaddr, busnum=self.i2cbusmode)
        self.seg2 = SevenSegment.SevenSegment(address=self.secsegaddr, busnum=self.i2cbusmode)
        self.secismpg = False
        self.btnqueue = btnqueue
        self.brightness = 7

        # GET THE CONFIG
        # OPEN THE JSON BASED CONFIG FILE
        f = open('/etc/carhud.cfg', 'r')
        lines = f.readlines()
        f.close()
        cstring = ""
        for line in lines:
            cstring += line
        # LOAD THE DATA INTO THE JSON STRING CONVERTOR
        self.config = json.loads(cstring)

        # ELM DEVICE
        self.myelm = FakeELM327()
        #self.myelm = elm327.ELM327(self.config['elm327info']['port'],self.config['elm327info']['baud'],units=self.config['elm327info']['units'])
        #self.myelm.setcalcmpg(self.config['elm327info']['calc mpg'])
        #self.myelm.setuplooprun(self.config['elm327info']['commands'],self.config['elm327info']['intercmddelay'],self.config['elm327info']['loopdelay'])

        # START THE DISPLAYS
        self.seg1.begin()
        self.seg2.begin()

        # SET INITIAL BRIGHTNESS
        self.seg1.set_brightness(self.brightness)
        self.seg2.set_brightness(self.brightness)

        # TURN OFF THE COLONS
        self.seg1.set_colon(False)
        self.seg2.set_colon(False)

        # ==========================================================================

    def kill(self):
        self.dead = True

    def run(self):

        # CLEAR DISPLAYS
        self.seg1.clear()
        self.seg2.clear()

        # DEFAULT TO 0.0 FOR SPEED AND 0 FOR RPM
        self.seg1.print_float(0.0,1)
        self.seg1.write_display()
        self.seg2.print_number_str("0")
        self.seg2.write_display()

        # OPEN ELM DEVICE
        #self.myelm.open()

        # CONNECT TO THE VEHICLE
        #resp = self.myelm.connect()
        #if resp[1] != "connected":
        #    sys.exit(-1)

        # START ELM LOOP
        self.myelm.start()

        # START MAIN LOOP FOR PYGAME
        # IN A TRY BLOCK SO ANY FAULT
        # WILL KILL THE GUI
        try:
            while not self.dead:

                # CHECK FOR GPIO EVENTS
                print "BIG LOOP: CHECK GPIO"
                self.check_gpio_events()

                # UPDATE TEXT
                print "BIG LOOP: UPDATE TEXT"
                self.update_text_displays()

                time.sleep(0.1)

        except KeyboardInterrupt:
            # GRACEFULLY CLOSE STUFF
            print "EXITING!!!!!!!"
            sys.exit()

    def update_text_displays(self):
        print "IN UPDATE TEXT DISPLAYS"
        # GET THE DATA FROM THE ELM DEVICE QUEUES
        print "LENGTH OF OBD QUEUE LIST: ", len(self.myelm.obdrespqueuelist)
        for obdresp in self.myelm.obdrespqueuelist:
            if obdresp.full():
                tmp = obdresp.get()
                if tmp['response'] != "no data":
                    # SPEED
                    if tmp['name'] == 'vss':
                        print "GOT SPEED DATA", tmp['response']
                        self.seg1.clear()
                        self.seg1.print_float(tmp['response'],1)
                        self.seg1.write_display()
                    elif tmp['name'] == 'rpm' and not self.secismpg:
                        print "GOT RPM DATA", tmp['response']
                        self.seg2.clear()
                        self.seg2.print_number_str(str(int(tmp['response'])))
                        self.seg2.write_display()
            else:
                 pass


        # DO MPG
        print "ARE WE DOING MPG: ", self.secismpg
        if self.config['elm327info']['calc mpg'] and self.secismpg:
            if self.myelm.mpgqueue.full():
                tmp = self.myelm.mpgqueue.get()
                if tmp['response'] != "no data":
                    print "HAVE MPG DATA", tmp['response']
                    self.seg2.clear()
                    self.seg2.print_float(tmp['response'],2)
                    self.seg2.write_display()

    def check_gpio_events(self):
        if self.btnqueue.full():
            data = self.btnqueue.get()
            if data == "TGL":
                # TOGGLE BOOLEAN FOR DISPLAYING MPG
                self.secismpg = not self.secismpg
                # CLEAR THE SECOND DISPLAY
                self.seg2.clear()
                self.seg2.write_display()
            elif data == "INC":
                self.brightness += 1
                if self.brightness > MAXBRIGHTNESS:
                    self.brightness = MAXBRIGHTNESS
            elif data == "DEC":
                self.brightness -= 1
                if self.brightness < MINBRIGHTNESS:
                    self.brightness = MINBRIGHTNESS

            # SEND THE NEW BRIGHTNESS
            if data != "TGL":
                self.seg1.set_brightness(self.brightness)
                self.seg2.set_brightness(self.brightness)
        else:
            pass

def Main():
    mypid = os.getpid()
    f = open('/tmp/carhud_dual7seg.pid', 'w')
    f.write(str(mypid))
    f.close()

    # SETUP GPIO
    SetupGPIO()

    # CREATE HUD AND START
    myhud = HUD(ButtonPress)

    # SLEEP FOR 5 SECONDS TO ALLOW INIT TEXT TO DISPLAY ON LEDS
    time.sleep(5)

    # NOW START
    myhud.run()

# SCRIPT MAIN
if __name__ == "__main__":
    Main()
