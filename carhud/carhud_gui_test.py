#!/usr/bin/env python

"""
CARHUD TEST GUI
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pycarhud

pygame based GUI for the CARHUD system

CHANGELOG:
 07/12/2015 - Added in capability for testing the MPG
 03/12/2014 - Added capability for formatting the data to display via strings
              in carhud.cfg json
              Added capability for config to over-ride programmed window size
              and text color
 12/19/2013 - Changed around the ELM command stuff in the carhud.cfg json
              so that required a little change to pull the right data from
              the proper location
 11/30/2013 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
from carhud_uses.textersprite import TexterSprite
import time
from pygame.locals import *
import pygame
import os
import sys
import json

# GLOBALS
SCREENWIDTH = 320
SCREENHEIGHT = 240
BGCOLOR = (0, 0, 0)
TEXTCOLOR = (0x00, 0xFB, 0x00)


# GUI CLASS
class HUD:
    def __init__(self, WW=640, WH=480):
        # CLASS VARIABLES
        self.ww = WW
        self.wh = WH
        self.bgcolor = BGCOLOR
        self.textcolor = TEXTCOLOR
        self.dead = False  # MAIN LOOP CONTROL VARIABLE
        self.initialread = True
        self.huddataformats = []
        self.oldresp = {}
        self.mpglistpos = 0

        # GET THE CONFIG
        # OPEN THE JSON BASED CONFIG FILE
        f = open('/etc/carhud.cfg', 'r')
        lines = f.readlines()
        f.close()
        cstring = ""
        for line in lines:
            cstring += line
        # LOAD THE DATA INTO THE JSON STRING CONVERTOR
        self.config = json.loads(cstring)
        #print self.config

        # IF WE HAVE A hud ELEMENT IN THE CONFIG
        if "hud" in self.config.keys():
            self.ww = self.config['hud']['width']
            self.wh = self.config['hud']['height']
            self.bgcolor = tuple(self.config['hud']['bg color'])
            self.textcolor = tuple(self.config['hud']['text color'])

        # INIT PYGAME
        pygame.init()

        # HIDE THE MOUSE
        pygame.mouse.set_visible(False)

        # SETUP THE TIMER
        self.timer = pygame.time.Clock()

        # CREATE THE SCREEN
        self.screen = pygame.display.set_mode((self.ww, self.wh), pygame.FULLSCREEN)
        self.background = pygame.Surface(self.screen.get_size(), SWSURFACE)
        self.background = self.background.convert()
        self.background.fill(self.bgcolor)
        self.screen.blit(self.background, (0, 0))

        # FLIP THE SCREEN
        pygame.display.update()

        # HUD SETUP
        # ==========================================================================

        # CREATE THE TEXT SPRITE GROUP
        self.hud_text = pygame.sprite.RenderUpdates()
        self.hudinfolist = []
        # CREATE THE TEXT SPRITES FOR HUD DATA - DEFAULT TO NO TEXT
        for pdict in self.config['elm327info']['commands']:
            if pdict['print'] and pdict['position'] != -1:
                tmpdict = self.config['elm327info']['positions'][pdict["position"]]
                tmppos = (tmpdict['x'], tmpdict['y'])
                tmptxt = tmpdict['format'] % 0
                self.huddataformats.append(tmpdict['format'])
                self.hudinfolist.append(TexterSprite(tmppos,self.textcolor,fontsize=tmpdict['fontsize'],text=tmptxt))

        # ADD MPG IF WE WANT
        if self.config['elm327info']['calc mpg']:
            tmpdict = self.config['elm327info']['mpg position']
            tmppos = (tmpdict['x'], tmpdict['y'])
            tmptxt = tmpdict['format'] % 0
            self.huddataformats.append(tmpdict['format'])
            self.hudinfolist.append(TexterSprite(tmppos,self.textcolor,fontsize=tmpdict['fontsize'],text=tmptxt))
            self.mpglistpos = len(self.hudinfolist) - 1

        for texter in self.hudinfolist:
            self.hud_text.add(texter)

        # DETERMINE IF WE'RE DISPLAYING FPS
        if self.config['extras']['fps display']:
            tmppos = (self.config['extras']['fps position']['x'],self.config['extras']['fps position']['y'])
            tmptxt = self.config['extras']['fps position']['format'] % 0.0
            self.fpstext = TexterSprite(tmppos,self.textcolor,fontsize=self.config['extras']['fps position']['fontsize'],text=tmptxt)
            self.hud_text.add(self.fpstext)

        # DETERMINE IF WE'RE DISPLAYING ELM-327 STATUS
        if self.config['extras']['status display']:
            tmppos = (self.config['extras']['status position']['x'],self.config['extras']['status position']['y'])
            tmptxt = self.config['extras']['status position']['format'] % "status position"
            self.statustext = TexterSprite(tmppos,self.textcolor,fontsize=self.config['extras']['status position']['fontsize'],text=tmptxt)
            self.hud_text.add(self.statustext)

        # ==========================================================================

    def kill(self):
        self.dead = True

    def run(self):
        # HIDE THE MOUSE, JUST TO BE SAFE
        pygame.mouse.set_visible(False)

        # START MAIN LOOP FOR PYGAME
        # IN A TRY BLOCK SO ANY FAULT
        # WILL KILL THE GUI
        try:
            while not self.dead:

                # WAIT FOR TIMER
                self.timer.tick(60)

                # CHECK FOR EVENTS
                self.check_events()

                # UPDATE TEXT
                self.update_text_sprites()

                # UPDATE SPRITES
                for sprite in self.hud_text:
                    sprite.update()

                # RE-DRAW THE TEXT SPRITES
                self.hud_text.clear(self.screen, self.background)
                dirty = self.hud_text.draw(self.screen)

                # UPDATE SCREEN
                pygame.display.update(dirty)
        finally:
            # GRACEFULLY CLOSE STUFF
            pygame.quit()
            sys.exit()

    def update_text_sprites(self):
        # DO MPG
        if self.config['elm327info']['calc mpg']:
            tmptext = self.huddataformats[self.mpglistpos] % 0.0
            self.hudinfolist[self.mpglistpos].updateText(tmptext)
        
        # DETERMINE IF WE ARE PRINTING FPS
        # IF SO, UPDATE IT
        if self.config['extras']['fps display']:
            tmp = self.config['extras']['fps position']['format'] % (self.timer.get_fps())
            self.fpstext.updateText(tmp)

        # DETERMINE IF WE ARE DISPLAYING ELM-327 STATUS
        if self.config['extras']['status display']:
            tmp = self.config['extras']['status position']['format'] % ("status position")
            self.statustext.updateText(tmp)

    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.dead = True

def Main():
    mypid = os.getpid()
    f = open('/tmp/carhud_gui.pid', 'w')
    f.write(str(mypid))
    f.close()

    # CREATE HUD
    myhud = HUD(SCREENWIDTH, SCREENHEIGHT)
    # RUN THE PROGRAM FOREVER
    myhud.run()

# SCRIPT MAIN
if __name__ == "__main__":
    Main()

