#!/usr/bin/env python

"""
TEXTER SPRITE
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pycarhud

Sprite for the CarHUD GUI client that will render text

CHANGELOG:
 11/08/2013 - Fix issue where the text wasn't centered on the x,y location
              specified in the constructor
 11/01/2013 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
import pygame


class TexterSprite(pygame.sprite.Sprite):

    def __init__(self, xy, color=(255,255,255), fontname=None, fontsize=25, text=''):
        # INIT THE INHERITED CLASS
        pygame.sprite.Sprite.__init__(self)
        # CLASS VARIABLES
        self.xy       = xy    # save xy -- will center our rect on it when we change the text
        self.fontsize = fontsize
        self.fontname = fontname
        self.color    = color
        self.text     = text
        self.font     = pygame.font.Font(self.fontname, self.fontsize)
        self.image    = self.font.render(self.text, True, self.color)
        self.rect     = self.image.get_rect()
        self.rect.center = self.xy
        self.old      = self.rect

    def updateText(self, text, xy=None):
        """Updates the text. Renders a new image and re-centers at the initial coordinates."""
        self.old = self.rect
        self.text = text
        self.image = self.font.render(self.text, True, self.color)
        self.rect = self.image.get_rect()
        # UPDATE POSITION IF WE HAVE NEW COORDINATES
        if xy is not None:
            self.xy = xy
        # RECENTER TEXT
        self.rect.center = self.xy
