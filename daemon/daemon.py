#!/usr/bin/env python

"""

This is not my original code, only uploading because it's required by the CARHUD server
https://bitbucket.org/xtacocorex/pycarhud

CODE FROM: http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/
BEFORE_START AND BEFORE_STOP IDEA FROM: http://www.cuddon.net/2013/01/a-simple-linux-daemon-written-in-python.html

CHANGELOG:
 10/02/2013 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
import sys
import os
import time
import atexit
from signal import SIGTERM

class Daemon:
    """
    A generic daemon class.
    
    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
    
    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced 
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        #sys.stdout.write('in daemonize'+os.linesep)
        try: 
            #sys.stdout.write('fork 1'+os.linesep)
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0) 
        except OSError, e: 
            sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)
    
        # decouple from parent environment
        os.chdir("/") 
        os.setsid()
        os.umask(0)
    
        # do second fork
        try: 
            #sys.stdout.write('fork 2'+os.linesep)
            pid = os.fork() 
            if pid > 0:
                # exit from second parent
                sys.exit(0)
        except OSError, e: 
            sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1) 
    
        # redirect standard file descriptors
        #sys.stdout.write('redirecting text output'+os.linesep)
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin, 'r')
        so = file(self.stdout, 'a+')
        se = file(self.stderr, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())
    
        # write pidfile
        #sys.stdout.write('creating pid file'+os.linesep)
        atexit.register(self.delpid)
        pid = str(os.getpid())
        file(self.pidfile, 'w+').write("%s\n" % pid)
    
    def delpid(self):
        os.remove(self.pidfile)

    def before_start(self):
        """
        Function to overload that enables super class to perform initial setup prior to main loop execution
        """
        pass

    def start(self):
        """
        Start the daemon
        """
        #sys.stdout.write('in start'+os.linesep)
        # Execute any code for starting up
        self.before_start()
        
        # Check for a pidfile to see if the daemon already runs
        #sys.stdout.write('daemon - looking for pid'+os.linesep)
        try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
    
        if pid:
            message = "pidfile %s already exist. Daemon already running?\n"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)
        
        # Start the daemon
        #sys.stdout.write('daemonize'+os.linesep)
        self.daemonize()
        #sys.stdout.write('calling run()'+os.linesep)
        self.run()

    def before_stop(self):
        """
        Function to overload that enables super class to perform post execution code prior to main loop stop
        """
        pass

    def stop(self):
        """
        Stop the daemon
        """
        
        # Execute any code for closing down properly
        self.before_stop()
        
        # Get the pid from the pidfile
        try:
            pf = file(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
    
        if not pid:
            message = "pidfile %s does not exist. Daemon not running?\n"
            sys.stderr.write(message % self.pidfile)
            return  # not an error in a restart

        # Try killing the daemon process    
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError, err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print str(err)
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """
        pass
