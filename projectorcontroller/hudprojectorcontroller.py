#!/usr/bin/env python

"""
HUD PROJECTOR CONTROLLER
Robert Wolterman (xtacocorex)
https://bitbucket.org/xtacocorex/pycarhud

Daemon based application that will deal
with control of the projector system

This requires:
 WiringPi2-Python or RPi-GPI as interfaces for GPIO
 smbus for I2C access

This will be started on bootup with an init script

CHANGELOG:
 01/17/2014 - Uploading fixes from initial hardware integration
              on the breadboard
 10/12/2013 - Initial upload to bitbucket git repo

"""

# MODULE IMPORTS
import time
import sys
import os
import wiringpi2
import RPi.GPIO as GPIO
import smbus
from daemon import daemon


class HUDProjectorController(daemon.Daemon):

    """
    HUD Projector Controller

    Controlling code for dealing with the HUD Projector.
    Script is set to run as a daemon

    """

    # HT16K33 16*8 LED I2C DEVICE
    # http://www.adafruit.com/datasheets/ht16K33v110.pdf
    # NOT USING A SEPARATE CLASS AS THAT WILL COMPLICATE THINGS
    # DEVICE REGISTERS
    DISPLAY_VALUE_REG = 0x00
    SYSTEM_SETUP_REG = 0x20
    DISPLAY_SETUP_REG = 0x80
    DIMMING_REG = 0xE0
    # DEVICE CONSTANTS
    SYSTEM_ENABLE = 0x01
    SYSTEM_STANDBY = 0x00
    DISPLAY_ENABLE = 0x01
    DISPLAY_DISABLE = 0x00
    BLINK_OFF = 0x00
    BLINK_2HZ = 0x01
    BLINK_1HZ = 0x02
    BLINK_HALFHZ = 0x03
    # DUMMY BYTE IS REQUIRED FOR I2C ACCESS
    DUMMYBYTE = 0x00

    # I2C ADDRESSES FOR THE 8x8 LED MATRICES
    # THE I2C CONTROLLER ALLOWS FOR 4 OF THE
    # DEVICES ON THE BUS 0x70 TO 0x73
    # http://learn.adafruit.com/adafruit-led-backpack/0-8-8x8-matrix
    # http://learn.adafruit.com/adafruit-led-backpack/changing-i2c-address
    LEDADDRESSES = [0x70, 0x71] #,0x72, 0x73]
    # MIN/MAX BRIGHTNESS LEVELS
    MINBRIGHTNESS = 0x00
    MAXBRIGHTNESS = 0x0F
    # MIN/MAX LED MODES
    MINLEDMODE = 0x00
    MAXLEDMODE = 0x05
    MODES = [
        [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
        [0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA],
        [0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55],
        [0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99, 0x99],
        [0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81],
        [0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE]
    ]
    MODESTRING = ["all on", "alt 1", "alt 2", "alt 3", "alt 4", "alt 5"]
    # GPIO PINS - RASPBERRY PI REV A MODEL B
    UP = 23
    DOWN = 24
    MODE = 25
    # PIN MODES - WIRING PI
    INPUT = 0
    OUTPUT = 1
    # SLEEP VALUES
    WIPISLEEP = 100
    RPGIOSLEEP = 0.1

    def __init__(self, i2cbusmode, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):

        # INITIALIZE INHERITED CLASS
        daemon.Daemon.__init__(self, pidfile, stdin, stdout, stderr)

        # SET THE I2C BUS MODE
        self.i2cbusmode = i2cbusmode

        # DEFAULT HUD BRIGHTNESS
        self.brightness = 7  # MEDIAN BRIGHTNESS
        self.ledmode = 0  # ALL ON

        # SETUP WIRING PI PIN SETUP
        #wiringpi2.wiringPiSetupGpio()
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()

        # SETUP OUR GPIO MODES
        #wiringpi2.pinMode(self.UP, self.INPUT)
        #wiringpi2.pinMode(self.DOWN, self.INPUT)
        #wiringpi2.pinMode(self.MODE, self.INPUT)
        GPIO.setup(self.UP, GPIO.IN)
        GPIO.setup(self.DOWN, GPIO.IN)
        GPIO.setup(self.MODE, GPIO.IN)

        # SETUP I2C
        self.i2cbus = smbus.SMBus(self.i2cbusmode)
        # CONFIGURE THE LEDS
        for addr in self.LEDADDRESSES:
            # ENABLE
            self.i2cbus.write_byte_data(addr, self.SYSTEM_SETUP_REG | self.SYSTEM_ENABLE, self.DUMMYBYTE)
            # SET BLINK RATE
            self.i2cbus.write_byte_data(addr, self.DISPLAY_SETUP_REG | (self.BLINK_OFF << 1) | self.DISPLAY_ENABLE,
                                        self.DUMMYBYTE)
            # SET BRIGHTNESS LEVEL
            self.i2cbus.write_byte_data(addr, self.DIMMING_REG | self.brightness, self.DUMMYBYTE)
            # SET THE LED MODE
            self.i2cbus.write_i2c_block_data(addr, self.DISPLAY_VALUE_REG, self.MODES[self.ledmode])

    def run(self):

        try:

            # RUN FOREVER
            while True:

                # CHECK GPIO, BUTTON PRESS WILL BE ACTIVE LOW
                # ACT ON INPUT (IF THERE IS ANY)
                #if not wiringpi2.digitalRead(self.UP):
                if GPIO.input(self.UP) == False:
                    # INCREMENT BRIGHTNESS
                    self.brightness += 1
                    if self.brightness > self.MAXBRIGHTNESS:
                        self.brightness = self.MAXBRIGHTNESS

                    # DEBUG
                    sys.stdout.write('increase brightness, level @ '+str(self.brightness)+os.linesep)

                    # DO I2C WRITE
                    for addr in self.LEDADDRESSES:
                        self.i2cbus.write_byte_data(addr, self.DIMMING_REG | self.brightness, self.DUMMYBYTE)

                #if not wiringpi2.digitalRead(self.DOWN):
                if GPIO.input(self.DOWN) == False:
                    # DECREMENT BRIGHTNESS
                    self.brightness -= 1
                    if self.brightness < self.MINBRIGHTNESS:
                        self.brightness = self.MINBRIGHTNESS

                    # DEBUG
                    sys.stdout.write('decrease brightness, level @ '+str(self.brightness)+os.linesep)
    
                    # DO I2C WRITE
                    for addr in self.LEDADDRESSES:
                        self.i2cbus.write_byte_data(addr, self.DIMMING_REG | self.brightness, self.DUMMYBYTE)

                #if not wiringpi2.digitalRead(self.MODE):
                if GPIO.input(self.MODE) == False:
                    # CYCLE THROUGH MODE
                    self.ledmode += 1
                    if self.ledmode > self.MAXLEDMODE:
                        self.ledmode = self.MINLEDMODE

                    # DEBUG
                    sys.stdout.write('current led mode '+self.MODESTRING[self.ledmode]+os.linesep)

                    # DO I2C WRITE
                    for addr in self.LEDADDRESSES:
                        self.i2cbus.write_i2c_block_data(addr, self.DISPLAY_VALUE_REG, self.MODES[self.ledmode])

                # SLEEP SO WE AREN'T RUNNING TOO FAST
                time.sleep(self.RPGIOSLEEP)
                #wiringpi2.delay(self.WIPISLEEP)

        except KeyboardInterrupt:
            GPIO.cleanup()
            #pass

# MAIN
if __name__ == "__main__":

    # I HAVE AN OLDER RPI, IF YOU HAVE A NEW ONE
    # THIS NEEDS TO BE SET TO 1
    RPIBUS = 0

    # CREATE OUR PROJECTOR CONTROLLER SERVER
    # THE PATHS HERE ARE OK, FOR YOCTO BUILD /tmp IS LINKED TO A
    # VOLATILE LOCATION AND WON'T WRITE TO THE READ ONLY FILESYSTEM
    # THESE LOGS WILL BE LOST ON POWER DOWN
    projectorcontroller = HUDProjectorController(RPIBUS,
                                                 '/tmp/projectorcontroller.pid',
                                                 stdout='/tmp/projectorcontroller.log')
    #projectorcontroller.run()

    # DETERMINE SCRIPT OPTIONS
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            print "STARTING"
            projectorcontroller.start()
        elif 'stop' == sys.argv[1]:
            projectorcontroller.stop()
        elif 'restart' == sys.argv[1]:
            projectorcontroller.restart()
        else:
            print "unknown command"
            sys.exit(2)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)

