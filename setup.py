from distutils.core import setup

classifiers = ['Development Status :: 3 - Alpha',
               'Operating System :: POSIX :: Linux',
               'License :: OSI Approved :: GNU GPLv2',
               'Intended Audience :: Developers',
               'Programming Language :: Python :: 2.6',
               'Programming Language :: Python :: 2.7',
               'Topic :: Software Development',
               'Topic :: System :: Hardware']

# THE GIT REPO HAS A DIRECTORY FOR CONFIG AND *NIX INIT SCRIPTS
# THESE ARE NOT ABLE TO BE INSTALLED WITH DISTUTILS IN YOCTO
# KEEPING THEM WITH THE REST OF THE CODE

# config/carhud.cfg NEEDS TO GO IN /etc
# initscripts/* NEED TO GO IN /etc/init.d AND THEN HAVE THE
#  APPROPRIATE RUNLEVELS ASSIGNED, FOR YOCTO PUT THE
#  SYMLINKS IN rc5.d (HAVEN'T VERIFIED THIS TO WORK YET)

setup(name             = "pyCARHUD",
      version          = "0.8",
      description      = "Python code for graphically displaying OBD-II data using pygame",
      long_description = open('README.txt').read(),
      author           = "Robert Wolterman",
      license          = "GNU GPLv2",
      author_email     = "robert.wolterman@gmail.com",
      url              = "https://bitbucket.org/xtacocorex/",
      packages         = ["daemon", "carhud_uses"],
      scripts          = ["carhud/carhud_gui.py",
                          "carhud/carhud_gui_test.py",
                          "carhud/carhud_dual7seg.py",
                          "carhud/carhud_dual7seg_test.py",
                          "carhud/carhud_dual7seg_init.sh",
                          "projectorcontroller/hudprojectorcontroller.py"]
      )
